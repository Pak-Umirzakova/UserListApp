﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Newtonsoft.Json;
using UserList.Domain.Entities;
using UserList.Models.Dto;

namespace UserList.JsonPlaceHolder.Integration
{
    public class JphProvider
    {
        static WebClient _client = new WebClient();
        private static IMapper _mapper;

        JphProvider(IMapper mapper)
        {
            _mapper = mapper;
        }

        public static async Task<UserDto> GetUser(int id, bool albums)
        {
            var url = String.Format("http://jsonplaceholder.typicode.com/users/{0}", id);
            var response = _client.DownloadString(url);
            if (response != null)
            {
                User user = await Task.Run(() => JsonConvert.DeserializeObject<User>(response));

                var responseAlbums = _client.DownloadString("http://jsonplaceholder.typicode.com/albums");
                if (responseAlbums != null && albums == true)
                {
                    var albumsEntities = await Task.Run(() => JsonConvert.DeserializeObject<ICollection<Album>>(responseAlbums));
                    user.Albums = albumsEntities.Where(r => r.UserId == user.Id).ToList() ?? null;
                }
                return Mapper.Map<UserDetailDto>(user);
            }
            else
            {
                return null;
            }
        }

        public static async Task<IEnumerable<UserDto>> GetUsers(bool isAddress, bool isCompany)
        {
            var response = await _client.DownloadStringTaskAsync("http://jsonplaceholder.typicode.com/users");
            if (response != null)
            {
                var users = await Task.Run(() => JsonConvert.DeserializeObject<ICollection<User>>(response));
                ICollection<UserDto> userDtos = new List<UserDto>();
                foreach (var user in users)
                {
                    if (isAddress == false)
                    {
                        user.Address = null;
                        user.AddressId = null;
                    }
                    if (isCompany == false)
                    {
                        user.Company = null;
                        user.CompanyId = null;
                    }
                    userDtos.Add(Mapper.Map<UserDto>(user));
                }
                return userDtos;
            }
            else return null;
        }
    }
}
