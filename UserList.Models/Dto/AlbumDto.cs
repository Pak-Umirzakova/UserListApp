﻿namespace UserList.Models.Dto
{
    public class AlbumDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
