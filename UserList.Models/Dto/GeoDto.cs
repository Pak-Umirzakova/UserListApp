﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserList.Models.Dto
{
    public class GeoDto
    {
        public string Lat { get; set; }
        public string Lng { get; set; }
    }
}
