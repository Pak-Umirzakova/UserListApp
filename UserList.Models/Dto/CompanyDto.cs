﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserList.Models.User
{
    public class CompanyDto
    {
        public string CompanyName { get; set; }
        public string CatchPhrase { get; set; }
        public string Bs { get; set; }
    }
}
