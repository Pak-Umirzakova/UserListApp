﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserList.Models.Dto
{
    public class UserDetailDto: UserDto
    {
        public ICollection<AlbumDto> Albums { get; set; }

        public UserDetailDto()
        {
            Albums = new List<AlbumDto>();
        }
    }
}
