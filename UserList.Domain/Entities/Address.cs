﻿using UserList.Domain.Abstract;

namespace UserList.Domain.Entities
{
	public class Address : IEntity
	{
		public int Id { get; set; }
		public string Street { get; set; }
		public string Suite { get; set; }
		public string City { get; set; }
		public string Zipcode { get; set; }
		public Geo Geo { get; set; }
		public int GeoId { get; set; }
		public User User { get; set; }
		public int UserId { get; set; }
	}
}
