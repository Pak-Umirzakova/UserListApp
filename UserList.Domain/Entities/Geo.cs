﻿using UserList.Domain.Abstract;

namespace UserList.Domain.Entities
{
	public class Geo:IEntity
	{
		public int Id { get; set; }
		public string Lat { get; set; }
		public string Lng { get; set; }
		public Address Address { get; set; }
		public int AddressId { get; set; }
	}
}