﻿using UserList.Domain.Abstract;

namespace UserList.Domain.Entities
{
    public class Album : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
    }
}
