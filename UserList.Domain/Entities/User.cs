﻿using System.Collections.Generic;
using UserList.Domain.Abstract;

namespace UserList.Domain.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public Address Address { get; set; }
        public int ?AddressId { get; set; }
        public Company Company { get; set; }
        public int ?CompanyId { get; set; }
        public ICollection<Album> Albums { get; set; }
        public ICollection<int> AlbumIds { get; set; }

        public User() {
            Albums = new List<Album>();
            AlbumIds = new List<int>();
        }
    }
}
