﻿using UserList.Domain.Abstract;

namespace UserList.Domain.Entities
{
	public class Company : IEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string CatchPhrase { get; set; }
		public string Bs { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
	}
}
