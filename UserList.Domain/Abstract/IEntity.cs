﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserList.Domain.Abstract
{
    public interface IEntity
    {
		int Id { get; set; }
    }
}
