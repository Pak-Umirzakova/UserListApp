﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UserList.JsonPlaceHolder.Integration;
using UserList.Models.Dto;

namespace UserListApp.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public async Task<IEnumerable<UserDto>> Get([FromQuery] bool address=false, bool company=false)
        {
            return await JphProvider.GetUsers(address, company);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<UserDto> Get(int id, [FromQuery] bool albums = false)
        {
            return await JphProvider.GetUser(id, albums);
        }

        // GET api/values/getsourcezip
        [HttpGet("getsourcezip")]
        public byte[] GetSourceZip()
        {
            string sourcePath = string.Format("{0}\\UserListApp.rar", Directory.GetCurrentDirectory());
            if (System.IO.File.Exists(sourcePath))
            {
                return System.IO.File.ReadAllBytes(sourcePath);
            }
            return null;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
