﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UserList.Domain.Entities;
using UserList.Models.Dto;
using UserList.Models.User;

namespace UserListApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<UserDto, User>().ReverseMap();
                cfg.CreateMap<UserDetailDto, User>().ReverseMap();
                cfg.CreateMap<GeoDto, Geo>().ReverseMap();
                cfg.CreateMap<AddressDto, Address>().ReverseMap();
                cfg.CreateMap<CompanyDto, Company>().ReverseMap()
                        .ForMember(dest => dest.CompanyName,
                        src => src.MapFrom(x => $"{x.Name}"));
                cfg.CreateMap<AlbumDto, Album>().ReverseMap();
            });
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
